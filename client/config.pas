unit config;

// ��������� VL:Screenshot
// ������ ������������
// �������� � 2011 ���� - �������� �. �.

interface

const
 RegPath : string = '\Software\VL\VLSS'; // ���� � ����� ������� �� HKEY_CURRENT_USER

type

 // ������ ������������
 TConfig = object
            // �������� - ��������� ������������
            // ��������� ��� ��������
            SendURL     : string;  // URL ��� �������� ���������
            SendLogin   : string;  // ����� ��� �������� ��������� (���� ������, �� ��� �����������)
            SendPwd     : string;  // ������ ��� �������� ���������
            ProxyServer : string;  // ������ ������ - ��� ������; auto - ���������������; ����� ������ �������
            ProxyLogin  : string;  // ����� ��� ������ (���� ������ ������ ��� ProxyServer=auto - ������ ��� �����������)
            ProxyPwd    : string;  // ������ ��� ������
            // ������
            procedure Init;   // ��������� �������� �� ���������
            procedure Read;   // ������ �������
            procedure Write;  // ������ �������
           end;

implementation

uses Registry;

procedure TConfig.Init;
begin
 SendUrl := 'http://www.example.com/sshots/load.php';
 SendLogin := '';
 SendPwd := '';
 ProxyServer := 'auto';
 ProxyLogin := '';
 ProxyPwd := '';
end;

procedure TConfig.Read;
var
 Reg : TRegistry;
 s   : string;
begin
 Init;
 Reg := TRegistry.Create;
 // ���� ���� ���������� � �����������,
 if Reg.OpenKey (RegPath,False)
  then begin
        // ������ ���������
        s := Reg.ReadString ('SendURL');
        if s <> '' then SendUrl := s;
        s := Reg.ReadString ('SendLogin');
        if s <> '' then SendLogin := s;
        s := Reg.ReadString ('SendPwd');
        if s <> '' then SendPwd := s;
        s := Reg.ReadString ('ProxyServer');
        if s <> '' then ProxyServer := s;
        s := Reg.ReadString ('ProxyLogin');
        if s <> '' then ProxyLogin := s;
        s := Reg.ReadString ('ProxyPwd');
        if s <> '' then ProxyPwd := s;
        // ��������� ���� �������
        Reg.CloseKey;
       end;
 Reg.Free;
end;

procedure TConfig.Write;
var
 Reg : TRegistry;
begin
 Reg := TRegistry.Create;
 // ��������� ����; ���� ��� ���, ������
 if Reg.OpenKey (RegPath,True)
  then begin
        Reg.WriteString ('SendURL',SendURL);
        Reg.WriteString ('SendLogin',SendLogin);
        Reg.WriteString ('SendPwd',SendPwd);
        Reg.WriteString ('ProxyServer',ProxyServer);
        Reg.WriteString ('ProxyLogin',ProxyLogin);
        Reg.WriteString ('ProxyPwd',ProxyPwd);
        // ��������� ���� �������
        Reg.CloseKey;
       end;
 Reg.Free;
end;

end.
