unit xmlwork;

// ������ ��������� XML
// 2011 ��� - �������� �. �.

interface

type

 // ��� �������� XML
 TXMLAttr = record
             Name : ANSIString;        // ������������ ��������
             Value : ANSIString;       // �������� ��������
            end;

 TXMLAttrArr = array of TXMLAttr;

 // ��� �������� XML
 TXMLElement = class
                Name  : ANSIString;                  // ������������ ��������
                Attrs : TXMLAttrArr;       // ��������
                Value : ANSIString;                  // �������� ������ ��������
                Child : array of TXMLElement;    // �������� ��������
                constructor Create;
                destructor Destroy;
                function Parse : Boolean;        // ������ ��������, ������������ �������� ���������
               end;

 // ��� ����� XML
 TXMLFile = class
             Attrs   : TXMLAttrArr;          // �������� �����
             Data    : ANSIString;               // ������ ����� XML
             Tag     : TXMLElement;          // �������� ������� XML
             constructor Create;
             destructor Destroy;
             function Parse : Boolean;       // ������ ������, ������������ �������� ���������
             function ReadFromFile (FileName : string) : Boolean; // ������ ������ �� ����� XML
            end;

implementation

uses SysUtils;

// ������� ������������� ������
function trim (const Line : ANSIString) : ANSIString;
var
 r : ANSIString;
 b : Boolean;
begin
 r := Line;
 if (r <> '')
  then begin
        // � ������
        repeat
         b := False;
         if r <> '' then if (r [1] = #13) or (r [1] = #10) or (r [1] = #32) or (r [1] = #9) then b := True;
         if b then r := copy (r,2,Length (r)-1);
        until b = False;
        // � �����
        repeat
         b := False;
         if r <> '' then if (r [Length (r)] = #13) or (r [Length (r)] = #10) or (r [Length (r)] = #32) or (r [Length (r)] = #9) then b := True;
         if b then r := copy (r,1,Length (r)-1);
        until b = False;
       end;
 trim := r;
end;

// ������� ���������� ������� ��������� � ������, ������� �� ��������� �������
function strpos (substr,str : ANSIString; startpos : LongInt) : LongInt;
var
 l,m : LongInt;
begin
 l := 0;
 if startpos > Length (str)-Length (substr)+1 then Result := 0
 else begin
       for m := startpos to Length (str)-Length (substr)+1
        do if (Copy (str,m,Length (substr)) = substr)
         then begin
               l := m;
               Break;
              end;
       Result := l;
      end;
end;

// ������� ���������� ������������� � ����� ����������� ������ XML-������ � ������ ���������
// ������:
// 1. �������� ������� � ������ 13, 10 � 9 �� ������ � ����� 32 (������);
// 2. ������� ������� ����� >
function preProcess (Data : ANSIString) : ANSIString;
var
 i,i1 : integer;
 s    : ANSIString;
begin
 s := Data;
 if s <> ''
  then begin
        for i := 1 to Length (s)
         do if (s [i] = #13) or (s [i] = #10) or (s [i] = #9)
          then s [i] := #32;
        i := Pos (' >',s);
        while i <> 0
         do begin
             i1 := i;
             while (s [i1] = #32) and (i1 > 2) do Dec (i1);
             s := Copy (s,1,i1) + Copy (s,i+1,Length (s)-i);
             i := Pos (' >',s);
            end;
       end;
 preProcess := s;
end;

constructor TXMLElement.Create;
begin
 SetLength (Attrs,0);
end;

destructor TXMLElement.Destroy;
var
 i : LongInt;
begin
 SetLength (Attrs,0);
 if Length (Child) > 0 then for i := Low (Child) to High (Child) do Child [i].Destroy;
 inherited Destroy;
end;

function parseAttrs (s : ANSIString) : TXMLAttrArr;
var
 n,v       : ANSIString;
 a         : TXMLAttrArr;
 w,x       : LongInt;
 Quote,Equ : Boolean;
begin
 SetLength (a,0);
 n := '';
 v := '';
 Quote := False;
 Equ := False;
 for w := 1 to Length (s)
  do begin
      if (Quote = False)
       then begin
             if s [w] = '=' then Equ := not Equ
             else if s [w] = '"' then Quote := True
             else if ((s [w] = ' ') or (s [w] = #13) or (s [w] = #10) or (s [w] = #9) and (Quote = False))
              then begin
                    // �������� ������� �������
                    x := High (a)+1;
                    SetLength (a,x+1);
                    a [x].Name:=n;
                    a [x].Value:=v;
                    n := '';
                    v := '';
                    Equ := False;
                   end
              else if Equ = False then n := n + s [w] else v := v + s [w];
            end
       else begin
             if s [w] = '"' then Quote := False
             else v := v + s [w];
            end;
     end;
 if n<>''
  then begin
        x := High (a)+1;
        SetLength (a,x+1);
        a [x].Name:=n;
        a [x].Value:=v;
       end;
 parseAttrs := a;
end;

function TXMLElement.Parse : Boolean;
var
 a,b,c,d           : ANSIString;
 AttrArray         : TXMLAttrArr;
 l,m,n             : LongInt;
 Quote             : Boolean;
 getAttrs,GetValue : Boolean;
begin
 // ������ ������
 SetLength (Child,0);
 a := trim (Value);
 if a = '' then Parse := False
 else if (a [1] <> '<') then Parse := False
 else begin
       l := 1;
       repeat
        // �������� � b ��� �������� ����
        b := '';
        m := 1;
        repeat
         b := b + a [l+m];
         Inc (m);
        until (a [l+m] = ' ') or (a [l+m] = '/') or (a [l+m] = '>');
        // � c �������������� ������ ���������
        c := '';
        // � d �������������� ��������
        d := '';
        // � ����������� �� ������� ��������� � �������� ����������� �����. �����
        getAttrs := (a [l+m] = ' ');
        getValue := (a [l+m] <> '/');
        // ���� ���� ��������, �������� ������ � �������� ��������
        if getAttrs
         then begin
               Quote := False;
               c := '';
               Inc (m);
               repeat
                if a [l+m] = '"' then Quote := not Quote;
                c := c + a [l+m];
                Inc (m);
               until ((a [l+m] = '>') or (a [l+m] = '/') and (Quote = False)) or (l+m > Length (a));
               // ���� �� ������������� � ������, �� �����
               if l+m > Length (a)
                then begin
                      Parse := False;
                      Exit;
                     end
                else begin
                      // ����� ����������
                      // �������� ��������
                      AttrArray := parseAttrs (trim (c));
                      // ������������ getValue
                      if getValue then getValue := (a [l+m] <> '/');
                     end;
              end
         else SetLength (AttrArray,0);
        if getValue
         then begin
               // �������� ��������
               if a [l+m] = '>' then Inc (m);
               // ���� �� ����� ����������� ���
               n := strpos ('</'+b+'>',a,l+m);
               // ���� ������������ ���� ���, �� �������
               if n = 0
                then begin
                      Parse := False;
                      Exit;
                     end
                else begin
                      // ����� �������� ������
                      d := Copy (a,l+m,n-l-m);
                      // � ����������� � m ������� ������������ ����
                      Inc (m,Length (d));
                      Inc (m,Length (b));
                      Inc (m,2);
                     end;
              end
         else Inc (m);
        // ������������� ������������� ���������
        Parse := True;
        // ��������� ��, ��� ��� ������� ������, � ������ ������
        n := High (Child)+1;
        SetLength (Child,n+1);
        Child [n] := TXMLElement.Create;
        Child [n].Name:=b;
        Child [n].Attrs := AttrArray;
        Child [n].Value:=d;
        Child [n].Parse;
        // �����������
        Inc (l,m);
        Inc (l);
        while ((a [l] = ' ') or (a [l] = #13) or (a [l] = #10) or (a [l] = #9)) and (l < Length (a)) do Inc (l);
       until l >= Length (a);
      end;
end;

constructor TXMLFile.Create;
begin
 SetLength (Attrs,0);
end;

destructor TXMLFile.Destroy;
begin
 SetLength (Attrs,0);
 if Tag <> nil then Tag.Destroy;
 inherited Destroy;
end;

function TXMLFile.Parse : Boolean;
var
 a,b,c,d           : ANSIString;
 l,m               : LongInt;
 Quote             : Boolean;
 getAttrs,getValue : Boolean;
begin
 // ������� ������, ���� ���� ���������� �������������
 SetLength (Attrs,0);
 if Tag<>nil then Tag.Destroy;
 // ��������� ������� ������
 a := trim (Data);
 if Length (a) < 8 then Parse := False
 else if Copy (a,1,5) <> '<?xml' then Parse := False
 else begin
       l := strpos ('?>',a,6);
       if l = 0 then Parse := False
       else begin
             // �������� ������ ���������
             b := copy (a,7,l-7);
             // ������
             Attrs := parseAttrs (trim (b));
             // �������� ������ ������ � ����� ������ � �������������, ����� �� �������� � ��� �����
             b := trim (copy (a,l+2,Length (a)-l-1));
             b := preProcess (b);
             // ������� �� �� �������� ���
             if b [1] <> '<'
              then begin
                    Parse := False;
                    Exit;
                   end
              else begin
                    Tag := TXMLElement.Create;
                    // ������
                    // �������� � ����� - ��� � c
                    c := '';
                    l := 2;
                    repeat
                     c := c + b [l];
                     Inc (l);
                    until (b [l] = ' ') or (b [l] = '/') or (b [l] = '>') or (l > Length (b));
                    if l > Length (b) then Parse := False
                    else begin
                          // ��� - � c
                          // ���������, ���� �� �������� �������� � ��������
                          getValue := (b [l] <> '/');
                          getAttrs := (b [l] = ' ');
                          // ���� ���� �������� ��������, �� ��������
                          if getAttrs
                           then begin
                                 d := '';
                                 Inc (l);
                                 Quote := False;
                                 repeat
                                  if b [l]='"' then Quote := not Quote;
                                  d := d + b [l];
                                  Inc (l);
                                 until (((b [l] = '/') or (b [l] = '>')) and (Quote = False)) or (l > Length (b));
                                 if l > Length (b)
                                  then begin
                                        Parse := False;
                                        Exit;
                                       end
                                  else begin
                                        Tag.Attrs := parseAttrs (trim (d));
                                        // ������������ ��� ������������� getValue
                                        if b [l] = '/'
                                         then Inc (l);
                                       end;
                                 d := '';
                                end;
                          // ���� ���� �������� ��������, �� ��������
                          if getValue
                           then begin
                                 // � d
                                 d := '';
                                 Inc (l);
                                 m := strpos ('</'+c+'>',b,l);
                                 if m = 0
                                  then begin
                                        Parse := False;
                                        Exit;
                                       end
                                  else d := copy (b,l,m-l);
                                end;
                          // ����������� ����������
                          Tag.Name:=trim (c);
                          Tag.Value:=trim (d);
                          if Tag.Value <> '' then Tag.Parse;
                          Parse := True;
                         end;
                   end;
            end;
      end;
end;

function TXMLFile.ReadFromFile (FileName : string) : Boolean;
var
 f      : file;
 Buf    : array [1..64000] of ANSIChar;
 Readed : integer;
begin
 try
  AssignFile (f,FileName);
  FileMode := fmOpenRead;
  reset (f,1);
  Data := '';
  repeat
   BlockRead (f,Buf [1],64000,Readed);
   Data := Data + Copy (Buf,1,Readed);
  until Readed < 64000;
  CloseFile (f);
  ReadFromFile := True;
 except
  ReadFromFile := False;
 end;
end;

end.