unit Unit1;

// ��������� VL:Screenshot - �������� ������
// �������� � 2011 ���� - �������� �. �.

interface

uses
 Windows,SysUtils,Variants,Classes,Graphics,Controls,Forms,IdBaseComponent,IdComponent,IdTCPConnection,IdTCPClient,IdHTTP,ExtCtrls,IdAuthentication;

type
 TForm1 = class (TForm)
           IdHTTP       : TIdHTTP;   // ���������, ������� ����� ���������� ����� �� ������
           Screen_Image : TImage;    // �����, �� ������� �� �������� ��������, ��������� ����� �� ��������
           procedure OnMouseDown (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
           procedure OnMouseUp (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
           procedure OnShow (Sender: TObject);
           procedure OnClose (Sender: TObject; var Action: TCloseAction);
           procedure OnMouseMove (Sender: TObject; Shift: TShiftState; X, Y: Integer);
           procedure OnAuth (Sender: TObject; Authentication: TIdAuthentication;  var Handled: Boolean);
          private
           { Private declarations }
          public
           { Public declarations }
          end;

var
 Form1: TForm1;

implementation

{$R *.dfm}

uses pngimage,IdMultipartFormData,xmlwork,ShellAPI,proxy,config,ClipBrd;

var
 Conf        : TConfig;  // ������ ���������� ������������
 SelStarted  : Boolean;  // ����, ������������, ���������� �� ��������� ��������� �����������
 X1,Y1,X2,Y2 : integer;  // ���������� ����������� ��������� �����������
 Scr         : TBitmap;  // ��������� �� ������ ��� ������� ���������
 Clicked     : Boolean;  // ���� ��� ������� ����� ������ - ��� ��������� ������, ����� �� ������������ ������� �� ��������� ���


// ������� OnShow
procedure TForm1.OnShow (Sender: TObject);
var
 MinX,MinY,MaxX,MaxY,n : integer;
begin
 // ������ ������������
 Conf.Read;
 // ������������ ���������� ������� ����� ������������ ��� ������� ������ � ������ �������� �����
 MinX := 65535;
 MinY := 65535;
 MaxX := 0;
 MaxY := 0;
 for n := 0 to Screen.MonitorCount - 1
  do begin
      if Screen.Monitors [n].Left < MinX then MinX := Screen.Monitors [n].Left;
      if Screen.Monitors [n].Top < MinY then MinY := Screen.Monitors [n].Top;
      if Screen.Monitors [n].Left + Screen.Monitors [n].Width > MaxX then MaxX := Screen.Monitors [n].Left + Screen.Monitors [n].Width;
      if Screen.Monitors [n].Top + Screen.Monitors [n].Height > MaxY then MaxY := Screen.Monitors [n].Top + Screen.Monitors [n].Height;
     end;
 // ������ ������ ��� ������� ���������
 Scr := TBitmap.Create;
 // ������ ������ ����� ������ �� ������ ������� ��������� � ����� ��� � Scr
 Scr.Width := MaxX - MinX;
 Scr.Height := MaxY - MinY;
 BitBlt (Scr.Canvas.Handle,MinX,MinY,Scr.Width,Scr.Height,GetDC (0),0,0,SRCCOPY);
 // �������� ���������� �������� � Screen_Image, �� ����� ���������� ������ � ������ ��������
 Screen_Image.Width := Scr.Width;
 Screen_Image.Height := Scr.Height;
 Screen_Image.Picture.Bitmap.Assign (Scr);
 // ������������� ��������� "���������" ��� ��������� �������������� ��� ��������� ���������
 Screen_Image.Canvas.Pen.Color := clBlack;
 Screen_Image.Canvas.Pen.Style := psDash;
 // ���� ������� �� ����, �������� ���� �� ��� �������� ��� ����������� ������ ��������� ����� �� ����
 if Screen.MonitorCount > 1
  then begin
        Self.WindowState := wsNormal;
        Self.Left := MinX;
        Self.Top := MinY;
        Self.Width := Scr.Width;
        Self.Height := Scr.Height;
        Self.Activate;
       end;
 // ������������� ���� ������ ������ � False - ����������, ��� ������������ ��� �� ����� �������� ��������
 SelStarted := False;
 // ������������� ���� ��������� ������ � False
 Clicked := False;
end;


// ��������� �����������
procedure TForm1.OnAuth (Sender: TObject; Authentication: TIdAuthentication; var Handled: Boolean);
begin
 if Conf.SendLogin <> ''
  then begin
        Handled := True;
        Authentication.Username := Conf.SendLogin;
        Authentication.Password := Conf.SendPwd;
       end;
end;

// �������� ����
procedure TForm1.OnClose (Sender: TObject; var Action: TCloseAction);
begin
 // ����� ������
 Conf.Write;
 // ����������� ������ �� ����������
 Scr.Free;
end;

// ������� ������ ���� - ���� ��������� �� ������
procedure TForm1.OnMouseDown (Sender: TObject; Button: TMouseButton;  Shift: TShiftState; X, Y: Integer);
begin
 // ���� ������ ������ ������ ����, ������� �� ���������
 if Button = mbRight then Close
 else if not Clicked
  then begin
        // ����� �������� ���������� �����, � ������� ���������� �������� �����������
        X1 := X;
        Y1 := Y;
        // � ������������� ����, �� �������� ������� OnMouseMove ����������, ������ ��������� ���� ��� ���������� ��������
        SelStarted := True;
       end;
end;

// OnMouseMove �� �������� - ������������� select-�
procedure TForm1.OnMouseMove (Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
 // ���� ������������ ��������
 if SelStarted
  then begin
        // ������������� � Screen_Image �������� �������� �� Scr, ������ ����� ������� ����� ������������ ������������� ���������
        Screen_Image.Picture.Bitmap.Assign (Scr);
        // ������ ����� ������������� � ������ ������������
        Screen_Image.Canvas.FrameRect (Rect (X1,Y1,X,Y));
       end;
end;

// ���������� ������ ����
procedure TForm1.OnMouseUp (Sender: TObject; Button: TMouseButton;  Shift: TShiftState; X, Y: Integer);
var
 Dest      : TBitmap;
 Img       : TPngImage;
 t         : integer;
 PS,PB,ACU : string;
 DS,MS     : TMemoryStream;
 POSTdata  : TIdMultiPartFormDataStream;
 SL        : TStringList;
 XML       : TXMLFile;
begin
 // ������� ���� ��������� ��������� ���������
 SelStarted := False;
 // ���� �������� ������ ������, �������
 if Button = mbRight then Close
 else if not Clicked
  then begin
        // ����� ���������� � �������� ���������
        // ������������� ���� Clicked
        Clicked := True;
        // ������� ���������� ������� �� ������ ���������� ������ ���� � X2 � Y2
        X2 := X;
        Y2 := Y;
        // ������������� ���������� �������������� � ����������� �� ����, ��� ������, � ��� ������
        // � X1/Y1 - ����� ������� ����, � X2/Y2 - ������ ������
        // ���������� t ���������� ��� ������������� ��� ������ ����������
        if X1 > X2
         then begin
               t := X1;
               X1 := X2;
               X2 := t;
              end;
        if Y1 > Y2
         then begin
               t := Y1;
               Y1 := Y2;
               Y2 := t;
              end;
        // �������� ��������� �������� �� ������ ������, ���������� ��� ������� ��������� � ����������� � Scr
        // �������� ���������� � ����� ������ Dest
        Dest := TBitmap.Create;
        with Dest
         do begin
             Width := X2 - X1 + 1;
             Height := Y2 - Y1 + 1;
             Canvas.CopyMode := cmSrcCopy;
             Canvas.CopyRect (Rect (0,0,Width,Height),Scr.Canvas,Rect (X1,Y1,X2,Y2));
            end;
        // ����������� ���������� ����������� � png � ����� ��� � stream ��� ��������
        // ��� ����� ������ ������� Img ���� TPNGImage � MS ���� TMemoryStream
        Img := TPngImage.Create;
        Img.Assign (Dest);
        MS := TMemoryStream.Create;
        Img.SaveToStream (MS);
        // ���������� ��� �� ������ ������� Img � Dest
        Img.Free;
        Dest.Free;
        // ��������� ������ - ��������� ����������� png �� ������
        // ������ ����� ����������� IdHttp
        // ������ - ��������� ��������� ������ � ����������� � ������������ � �����������
        // ���� ProxyServer - ������ ������, ����������
        // ���� �������� ��������� Proxy auto , ��������
        if Conf.ProxyServer <> ''
         then if Conf.ProxyServer = 'auto'
          then begin
                // ���������� ��������� ������ ��� URL � ������� ������� WinHttpProxy � ��� ������� ������ ����������� IdHttp
                GetProxyForURL (Conf.SendURL,PS,PB,ACU);
                if PS <> ''
                 then begin
                       t := Pos (':',PS);
                       if t <> 0
                        then begin
                              IdHttp.ProxyParams.ProxyPort := StrToInt (Copy (PS,t+1,Length (PS)-t));
                              IdHttp.ProxyParams.ProxyServer := Copy (PS,1,t-1);
                             end;
                      end;
               end
          else begin
                // ���� ������ �������� �������, ����������� � ������ ��������� ������� � ��� ������������� �����������
                // ������ ������
                t := Pos (':',Conf.ProxyServer);
                if t <> 0
                 then begin
                       IdHttp.ProxyParams.ProxyPort := StrToInt (Copy (Conf.ProxyServer,t+1,Length (Conf.ProxyServer)-t));
                       IdHttp.ProxyParams.ProxyServer := Copy (Conf.ProxyServer,1,t-1);
                      end;
                // ������ ����������� (���� ����)
                if Conf.ProxyLogin <> ''
                 then begin
                       IdHttp.ProxyParams.BasicAuthentication := True;
                       IdHttp.ProxyParams.ProxyUsername := Conf.ProxyLogin;
                       IdHttp.ProxyParams.ProxyPassword := Conf.ProxyPwd;
                      end;
               end;
        // ������ ������ ����� POST-������ � �������� ���� ����
        POSTData := TIdMultiPartFormDataStream.Create;
        MS.Seek (0,soFromBeginning);
        POSTData.AddObject ('pic','image/png',MS,'sshot.png');
        // ������������� Content-Type ��� �������
        IdHTTP.Request.ContentType:='multipart/form-data';
        // ������ ������� ��� ����� ������ �� �������
        DS := TMemoryStream.Create;
        SL := TStringList.Create;
        // �������� ������ � try - except
        try
         IdHTTP.Post (Conf.SendURL,POSTData,DS);
         if IdHttp.ResponseCode = 200
          then begin
                // ������ ����� � SL
                DS.Seek (0,soFromBeginning);
                SL.LoadFromStream (DS);
                // � SL.Text � ��� ����� - ��� ����� ����������
                XML := TXMLFile.Create;
                XML.Data := SL.Text;
                //  ��� ������ �� �������� ���������� ��� �� ������ ��� ���������� PS
                PS := '';
                if not XML.Parse then MessageBox (Handle,'������ ������ �������� �����.','������',mb_IconError)
                else begin
                      // XML ���������� - ������ �������� ��������� ����
                      if XML.Tag.Name = 'picture'
                       then if Length (XML.Tag.Attrs) > 0
                        then if XML.Tag.Attrs [0].Name = 'link'
                         then PS := XML.Tag.Attrs [0].Value;
                      // ���� PS �� ������, �� � ��� ������ �� ��������
                      if PS = ''
                       then MessageBox (Handle,'����� �� ������� �� �������� ������ �� ��������.','������',mb_IconError)
                       else begin
                             // �������� ������ � ����� ������
                             Clipboard.Open;
                             Clipboard.SetTextBuf (PChar (PS));
                             Clipboard.Close;
                             // ��������� ������ � ��������
                             ShellExecute (Handle,'open',PChar (PS),'','',sw_maximize);
                            end;
                     end;
                // ��������� XML-������ - �� ��� ������ �� �����
                XML.Destroy;
               end
          else MessageBox (Handle,PChar ('������ ������ ������ '+IntToStr (IdHttp.ResponseCode)+' '+IdHttp.ResponseText),'������',mb_IconError);
        except
         MessageBox (Handle,'�� ������� ����������� � ��������.','������',mb_IconError);
        end;
        // �����������
        POSTData.Free;
        SL.Free;
        DS.Free;
        MS.Free;
        Close;
       end;
end;

end.
