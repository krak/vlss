unit winhttpproxy;

// ������� ��������� ������ � ���� winhttpproxy (�)������ � http://www.cyberforum.ru/delphi-networks/thread46823.html

interface

uses Windows;

type
 HINTERNET = Pointer;

 PWinHttpProxyInfo = ^TWinHttpProxyInfo;
 TWinHttpProxyInfo = record
                      dwAccessType    : DWORD;    // see WINHTTP_ACCESS_ types below
                      lpszProxy       : LPWSTR;   // proxy server list
                      lpszProxyBypass : LPWSTR;   // proxy bypass list
                     end;

 PWinHttpAutoProxyOptions = ^TWinHttpAutoProxyOptions;
 TWinHttpAutoProxyOptions = record
                             dwFlags                : DWORD;
                             dwAutoDetectFlags      : DWORD;
                             lpszAutoConfigUrl      : LPWSTR; //PWideChar;
                             lpvReserved            : Pointer;
                             dwReserved             : DWORD;
                             fAutoLogonIfChallenged : BOOL;
                            end;


 PWinHttpCurrentUserIEProxyConfig = ^TWinHttpCurrentUserIEProxyConfig;
 TWinHttpCurrentUserIEProxyConfig = record
                                     fAutoDetect       : boolean;
                                     lpszAutoConfigUrl : LPWSTR; //PWideChar;
                                     lpszProxy         : LPWSTR; //PWideChar;
                                     lpszProxyBypass   : LPWSTR; //PWideChar;
                                    end;

const
 winhttpdll = 'winhttp.dll';

 WINHTTP_ACCESS_TYPE_DEFAULT_PROXY       = 0;
 WINHTTP_ACCESS_TYPE_NO_PROXY            = 1;
 WINHTTP_ACCESS_TYPE_NAMED_PROXY         = 3;

 WINHTTP_NO_PROXY_NAME                   = nil;
 WINHTTP_NO_PROXY_BYPASS                 = nil;

 INTERNET_DEFAULT_PORT                   = 0;
 INTERNET_DEFAULT_HTTP_PORT              = 80;
 INTERNET_DEFAULT_HTTPS_PORT             = 443;
 INTERNET_SCHEME_HTTP                    = (1);
 INTERNET_SCHEME_HTTPS                   = (2);
 WINHTTP_AUTOPROXY_AUTO_DETECT           = $00000001;
 WINHTTP_AUTOPROXY_CONFIG_URL            = $00000002;
 WINHTTP_AUTOPROXY_RUN_OUTPROCESS_ONLY   = $00020000;
 WINHTTP_AUTO_DETECT_TYPE_DHCP           = $00000001;
 WINHTTP_AUTO_DETECT_TYPE_DNS_A          = $00000002;

 WINHTTP_ERROR_BASE = 12000;
 ERROR_WINHTTP_AUTO_PROXY_SERVICE_ERROR = (WINHTTP_ERROR_BASE + 178);
 ERROR_WINHTTP_BAD_AUTO_PROXY_SCRIPT = (WINHTTP_ERROR_BASE + 166);

 { prototypes }

//function WinHttpQueryOption(hInet: HINTERNET; dwOption: DWORD;
//  lpBuffer: Pointer; var lpdwBufferLength: DWORD): BOOL; stdcall;
//  {$EXTERNALSYM WinHttpQueryOption}

//function WinHttpDetectAutoProxyConfigUrl(dwAutoDetectFlags: DWORD;
//  var ppwszAutoConfigUrl: LPWSTR): BOOL; stdcall;
//  {$EXTERNALSYM WinHttpDetectAutoProxyConfigUrl}

function WinHttpGetDefaultProxyConfiguration (var pProxyInfo: PWinHttpProxyInfo): BOOL; stdcall;
 {$EXTERNALSYM WinHttpGetDefaultProxyConfiguration}

function WinHttpGetIEProxyConfigForCurrentUser (pProxyInfo: PWinHttpCurrentUserIEProxyConfig): BOOL; stdcall;
 {$EXTERNALSYM WinHttpGetIEProxyConfigForCurrentUser}

function WinHttpGetProxyForUrl (hSession: HINTERNET; lpcwszUrl: PWideChar; pAutoProxyOptions: PWinHttpAutoProxyOptions; pProxyInfo: PWinHttpProxyInfo): BOOL; stdcall;
 {$EXTERNALSYM WinHttpGetProxyForUrl}


function WinHttpCheckPlatform: BOOL; stdcall;
 {$EXTERNALSYM WinHttpCheckPlatform}

function WinHttpOpen (pwszUserAgent: PWideChar; dwAccessType: DWORD; pwszProxyName, pwszProxyBypass: PWideChar; dwFlags: DWORD): HINTERNET; stdcall;
 {$EXTERNALSYM WinHttpOpen}

function WinHttpCloseHandle (hInternet: HINTERNET): BOOL; stdcall;
 {$EXTERNALSYM WinHttpCloseHandle}


implementation

function WinHttpGetDefaultProxyConfiguration;
external winhttpdll name 'WinHttpGetDefaultProxyConfiguration';

function WinHttpGetIEProxyConfigForCurrentUser;
external winhttpdll name 'WinHttpGetIEProxyConfigForCurrentUser';

function WinHttpCheckPlatform;
external winhttpdll name 'WinHttpCheckPlatform';

function WinHttpOpen;
external winhttpdll name 'WinHttpOpen';

function WinHttpCloseHandle;
external winhttpdll name 'WinHttpCloseHandle';

function WinHttpGetProxyForUrl;
external winhttpdll name 'WinHttpGetProxyForUrl';

end.
