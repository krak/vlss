object Form1: TForm1
  Left = 0
  Top = 0
  Cursor = crCross
  BorderStyle = bsNone
  Caption = 'Form1'
  ClientHeight = 281
  ClientWidth = 413
  Color = clBtnFace
  TransparentColorValue = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnClose = OnClose
  OnMouseDown = OnMouseDown
  OnMouseUp = OnMouseUp
  OnShow = OnShow
  PixelsPerInch = 96
  TextHeight = 13
  object Screen_Image: TImage
    Left = 0
    Top = 0
    Width = 105
    Height = 105
    OnMouseDown = OnMouseDown
    OnMouseMove = OnMouseMove
    OnMouseUp = OnMouseUp
  end
  object IdHTTP: TIdHTTP
    AllowCookies = False
    HandleRedirects = True
    ProtocolVersion = pv1_0
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    HTTPOptions = [hoForceEncodeParams]
    OnAuthorization = OnAuth
    Left = 120
    Top = 8
  end
end
